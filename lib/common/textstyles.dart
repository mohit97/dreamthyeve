import 'package:dreamthyevecutomer/common/colors.dart';
import 'package:flutter/material.dart';
import 'constants.dart';

class TextStyles {
  //White
  static TextStyle h1White = TextStyle(
    color: whiteColor,
    fontFamily: 'Trueno',
    fontSize: h1,
  );
  static TextStyle h1WhiteBold = TextStyle(
    color: whiteColor,
    fontFamily: 'Trueno',
    fontSize: h1,
    fontWeight: FontWeight.bold,
  );

  static TextStyle h2White = TextStyle(
    color: whiteColor,
    fontFamily: 'Trueno',
    fontSize: h2,
  );
  static TextStyle h2WhiteBold = TextStyle(
    color: whiteColor,
    fontFamily: 'Trueno',
    fontSize: h2,
    fontWeight: FontWeight.bold,
  );

  static TextStyle h3White = TextStyle(
    color: whiteColor,
    fontFamily: 'Trueno',
    fontSize: h3,
  );
  static TextStyle h3WhiteBold = TextStyle(
    color: whiteColor,
    fontFamily: 'Trueno',
    fontSize: h3,
    fontWeight: FontWeight.bold,
  );

  static TextStyle h4White = TextStyle(
    color: whiteColor,
    fontFamily: 'Trueno',
    fontSize: h4,
  );
  static TextStyle h4WhiteBold = TextStyle(
    color: whiteColor,
    fontFamily: 'Trueno',
    fontSize: h4,
    fontWeight: FontWeight.bold,
  );

  static TextStyle t1White = TextStyle(
    color: whiteColor,
    fontFamily: 'Trueno',
    fontSize: t1,
  );
  static TextStyle t1WhiteBold = TextStyle(
    color: whiteColor,
    fontFamily: 'Trueno',
    fontSize: t1,
    fontWeight: FontWeight.bold,
  );

  static TextStyle t2White = TextStyle(
    color: whiteColor,
    fontFamily: 'Trueno',
    fontSize: t2,
  );
  static TextStyle t2WhiteBold = TextStyle(
    color: whiteColor,
    fontFamily: 'Trueno',
    fontSize: t2,
    fontWeight: FontWeight.bold,
  );

  static TextStyle t3White = TextStyle(
    color: whiteColor,
    fontFamily: 'Trueno',
    fontSize: t3,
  );
  static TextStyle t3WhiteBold = TextStyle(
    color: whiteColor,
    fontFamily: 'Trueno',
    fontSize: t3,
    fontWeight: FontWeight.bold,
  );

  static TextStyle t4White = TextStyle(
    color: whiteColor,
    fontFamily: 'Trueno',
    fontSize: t4,
  );
  static TextStyle t4WhiteBold = TextStyle(
    color: whiteColor,
    fontFamily: 'Trueno',
    fontSize: t4,
    fontWeight: FontWeight.bold,
  );

  //Pink
  static TextStyle h1Pink = TextStyle(
    color: pinkColor,
    fontFamily: 'Trueno',
    fontSize: h1,
  );
  static TextStyle h1PinkBold = TextStyle(
    color: pinkColor,
    fontFamily: 'Trueno',
    fontSize: h1,
    fontWeight: FontWeight.bold,
  );

  static TextStyle h2Pink = TextStyle(
    color: pinkColor,
    fontFamily: 'Trueno',
    fontSize: h2,
  );
  static TextStyle h2PinkBold = TextStyle(
    color: pinkColor,
    fontFamily: 'Trueno',
    fontSize: h2,
    fontWeight: FontWeight.bold,
  );

  static TextStyle h3Pink = TextStyle(
    color: pinkColor,
    fontFamily: 'Trueno',
    fontSize: h3,
  );
  static TextStyle h3PinkBold = TextStyle(
    color: pinkColor,
    fontFamily: 'Trueno',
    fontSize: h3,
    fontWeight: FontWeight.bold,
  );

  static TextStyle h4Pink = TextStyle(
    color: pinkColor,
    fontFamily: 'Trueno',
    fontSize: h4,
  );
  static TextStyle h4PinkBold = TextStyle(
    color: pinkColor,
    fontFamily: 'Trueno',
    fontSize: h4,
    fontWeight: FontWeight.bold,
  );

  static TextStyle t1Pink = TextStyle(
    color: pinkColor,
    fontFamily: 'Trueno',
    fontSize: t1,
  );
  static TextStyle t1PinkBold = TextStyle(
    color: pinkColor,
    fontFamily: 'Trueno',
    fontSize: t1,
    fontWeight: FontWeight.bold,
  );

  static TextStyle t2Pink = TextStyle(
    color: pinkColor,
    fontFamily: 'Trueno',
    fontSize: t2,
  );
  static TextStyle t2PinkBold = TextStyle(
    color: pinkColor,
    fontFamily: 'Trueno',
    fontSize: t2,
    fontWeight: FontWeight.bold,
  );

  static TextStyle t3Pink = TextStyle(
    color: pinkColor,
    fontFamily: 'Trueno',
    fontSize: t3,
  );
  static TextStyle t3PinkBold = TextStyle(
    color: pinkColor,
    fontFamily: 'Trueno',
    fontSize: t3,
    fontWeight: FontWeight.bold,
  );

  static TextStyle t4Pink = TextStyle(
    color: pinkColor,
    fontFamily: 'Trueno',
    fontSize: t4,
  );
  static TextStyle t4PinkBold = TextStyle(
    color: pinkColor,
    fontFamily: 'Trueno',
    fontSize: t4,
    fontWeight: FontWeight.bold,
  );
}
