import 'package:flutter/material.dart';

final whiteColor = Colors.white;
final blackColor = Colors.black;
final greyColor = Colors.grey;
final pinkColor = Color(0xFFD82C63);
