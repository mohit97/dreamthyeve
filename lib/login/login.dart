import 'package:dreamthyevecutomer/common/colors.dart';
import 'package:dreamthyevecutomer/common/display_helper.dart';
import 'package:dreamthyevecutomer/common/textstyles.dart';
import 'package:dreamthyevecutomer/signup/signup.dart';
import 'package:flutter/material.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: displayWidth(context),
        height: displayHeight(context),
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage(
                  'assets/bgimage.png',
                ),
                fit: BoxFit.cover)),
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                height: displayHeight(context) * 0.20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  RaisedButton(
                    color: pinkColor,
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Text(
                        'Login ',
                        style: TextStyles.t1White,
                      ),
                    ),
                    onPressed: () {},
                  ),
                  RaisedButton(
                    color: whiteColor,
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Text(
                        'SignUp',
                        style: TextStyles.t1Pink,
                      ),
                    ),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => SignUp(),
                          ));
                    },
                  ),
                ],
              ),
              SizedBox(
                height: displayHeight(context) * 0.05,
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: displayWidth(context) * 0.08),
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: whiteColor),
                  child: TextFormField(
                    controller: emailController,
                    keyboardType: TextInputType.emailAddress,
                    decoration: InputDecoration(
                      labelText: 'Email your Email',
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10)),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: displayHeight(context) * 0.03,
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: displayWidth(context) * 0.08),
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: whiteColor),
                  child: TextFormField(
                    controller: passwordController,
                    decoration: InputDecoration(
                      labelText: 'Email your Password',
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10)),
                    ),
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {},
                child: Align(
                  alignment: Alignment.centerRight,
                  child: Padding(
                    padding:
                        EdgeInsets.only(right: displayWidth(context) * 0.08),
                    child: Text('Forgot Password?', style: TextStyles.t3White),
                  ),
                ),
              ),
              SizedBox(
                height: displayHeight(context) * 0.02,
              ),
              RaisedButton(
                color: pinkColor,
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: Text(
                    'Login',
                    style: TextStyles.t1White,
                  ),
                ),
                onPressed: () {},
              ),
              SizedBox(
                height: displayHeight(context) * 0.02,
              ),
              GestureDetector(
                onTap: () {},
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text('or ', style: TextStyles.t1White),
                    Text(' Login via Phone Number', style: TextStyles.t1Pink),
                  ],
                ),
              ),
              SizedBox(
                height: displayHeight(context) * 0.02,
              ),
              Container(
                height: displayHeight(context) * 0.08,
                child: RaisedButton.icon(
                  elevation: 0,
                  color: whiteColor,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30),
                  ),
                  onPressed: () {},
                  icon: Image.asset(
                    'assets/google.png',
                    width: 30,
                    height: 30,
                    fit: BoxFit.scaleDown,
                  ),
                  label: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: Text('Login with Google   '),
                  ),
                ),
              ),
              SizedBox(
                height: displayHeight(context) * 0.03,
              ),
              Container(
                height: displayHeight(context) * 0.08,
                child: RaisedButton.icon(
                  elevation: 0,
                  color: whiteColor,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30),
                  ),
                  onPressed: () {},
                  icon: Image.asset(
                    'assets/facebook.png',
                    width: 30,
                    height: 30,
                    fit: BoxFit.cover,
                  ),
                  label: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: Text('Login with Facebook'),
                  ),
                ),
              ),
              SizedBox(
                height: displayHeight(context) * 0.06,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Looking for a business account?',
                    style: TextStyle(
                        decoration: TextDecoration.underline,
                        color: whiteColor),
                  ),
                  Text('  Click here', style: TextStyles.t2White),
                ],
              ),
              // SizedBox(
              //   height: displayHeight(context) * 0.045,
              // ),
            ],
          ),
        ),
      ),
    );
  }
}
